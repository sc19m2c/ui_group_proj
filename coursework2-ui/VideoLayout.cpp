//
// Created by sc19mvdl on 12/12/19.
//

#include <QtWidgets/QPushButton>
#include <QtWidgets/QLayout>
#include <iostream>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QtWidgets/QSlider>
#include <QtWidgets/QToolButton>
#include "VideoLayout.h"

using namespace std;

void VideoLayout::setGeometry(const QRect &r){
    QLayout::setGeometry(r);

    // place all the widgets on the right place in the layout

    int counter=0; //make sure the sliders will be treated differently
    for (int i = 0; i < list_.size(); i++) {
        QLayoutItem *o = list_.at(i);

        QPushButton* push = dynamic_cast<QPushButton*>(list_.at(i)->widget());
        if (push){
            push->setGeometry(QRect(r.x()+r.width()/2-60,r.y()+r.height()-115,60,60));
            continue;
        }
        QSlider* slide = dynamic_cast<QSlider*>(list_.at(i)->widget());
        if (slide&&counter%2==0){
            slide->setGeometry(QRect(r.x(),r.y()+r.height()-50,r.width(),50));
            counter++;
            continue;
        }
        QVideoWidget* video = dynamic_cast<QVideoWidget*>(list_.at(i)->widget());
        if (video){
            video->setGeometry(QRect(r.x(),r.y(),r.width(),r.height()));
            continue;
        }
        QToolButton* mute = dynamic_cast<QToolButton*>(list_.at(i)->widget());
        if (mute){
            mute->setGeometry(QRect(r.x()+r.width()-200,r.y()+r.height()-100,30,30));
            continue;
        }
        QSlider* volume = dynamic_cast<QSlider*>(list_.at(i)->widget());
        if (volume){
            volume->setGeometry(QRect(r.x()+r.width()-170,r.y()+r.height()-88,170,6));
            counter++;
            continue;
        }
    }
}

int VideoLayout::count() const {
    return list_.size();
}

QLayoutItem *VideoLayout::itemAt(int idx) const {
    return list_.value(idx);
}

QLayoutItem *VideoLayout::takeAt(int idx) {
    return idx >= 0 && idx < list_.size() ? list_.takeAt(idx) : 0;
}

void VideoLayout::addItem(QLayoutItem *item) {
    list_.append(item);
}


QSize VideoLayout::sizeHint() const {
    return minimumSize();
}

QSize VideoLayout::minimumSize() const {
    return QSize(320,320);
}

VideoLayout::~VideoLayout() {
    QLayoutItem *item;
    while ((item = takeAt(0)))
        delete item;
}