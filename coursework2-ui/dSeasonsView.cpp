//
// Created by sc19m2c on 06/12/19.
//

#include "dSeasonsView.h"
#include <iostream>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QScrollArea>
#include "readCsv.h"
#include "dNavbar.h"
#include "dSeasonsScroll.h"
#include "scrollWindow.h"


using namespace std;

void horizontalScroller(ScrollWindow *container, MyTomeo *myTomeo){

    container->setGeometry(QRect(0,100*(myTomeo->getSeasonNumber() - 1 ), 692, 250));
    //cout << myTomeo->getSeasonNumber() << endl;
    //container->setMaximumHeight(250);
    //container->setStyleSheet("background-color: black;");

    container->show();
    QList<QObject*> inside = container->children();

    // loop threough scroll windows children to find the scroll area
    for (int i = 0 ; i < inside.length() ; i ++){
        QScrollArea* scroll = dynamic_cast<QScrollArea*>(inside[i]);
        if (scroll){
            scroll->setMinimumSize(692,250);
            scroll->widget()->setMinimumSize(1200, 300 ); //// need a function to define the height

            scroll->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
            scroll->setWidgetResizable( true );

            //scroll->setMinimumSize(r.width()<MAX_RES_WIDTH? r.width(): MAX_RES_WIDTH, r.height() / 6);
            /*
            if (r.width()> MAX_RES_WIDTH){
                scroll->setGeometry(QRect(0,0,container->sizeHint().width(),r.height() / 6));
            }else{
                scroll->setGeometry(QRect(0,0,container->sizeHint().width(),r.height() / 6));
            }
            */
            continue;
        }
    }
}

void DseasonsView::setGeometry(const QRect &r){
    QLayout::setGeometry(r);

    // for all the Widgets added in ResponsiveWindow.cpp

    for (int i = 0; i < list_.size(); i++) {

        QLayoutItem *o = list_.at(i);


        MasterLayout* nav = dynamic_cast<Dnavbar*>(list_.at(i));
        if (nav){
            nav->setGeometry(QRect(r.x(),r.y(),r.width(),50));
            continue;
        }


        QLabel* seaslabel = dynamic_cast<QLabel*>(list_.at(i));
        if (seaslabel){
            seaslabel->setGeometry(10,50,150,50);
            continue;
        }

        ScrollWindow* scrollWin = dynamic_cast<ScrollWindow*>(list_.at(i)->widget());
        if(scrollWin){
            horizontalScroller(scrollWin,myTomeo);
            continue;
        }

        try {

        }
        catch (bad_cast) {
            cout << "warning, unknown widget class iQQQQQn layout" << i << endl;
        }
    }
}


void DseasonsView::createWidgets(){
    MasterLayout *nav = new Dnavbar(myTomeo);
    nav->createWidgets();
    addItem(nav);
    cout << "testing" <<endl;

    // gets the string name of the tv show clicked on from catagoires
    std::string tvShow_id = getTomeo()->getTvShow_id();
    char const *tvid = tvShow_id.c_str();
    ReadCsv *tvShows = new ReadCsv("/home/csunix/sc18mmyc/Downloads/ui_group_proj/coursework2-ui/tmp/TvShows.csv");
    tvShows->loadData();


    ReadCsv *allShows = new ReadCsv("/home/csunix/sc18mmyc/Downloads/ui_group_proj/coursework2-ui/tmp/allTvShows.csv");
    allShows->loadData();

    ReadCsv *series = allShows->getAllWhere("tvShow_id", tvid);

    int  maxSeas = 0;
    for(vector<string> data : series->getData()){
        string seasonInt = data[series->indexOf("season")];
        int currentSeas = std::stoi(seasonInt);
        if (currentSeas > maxSeas){
            maxSeas = currentSeas;
        }
    }
    cout << maxSeas << endl;

    for (int i = 1; i <= maxSeas; i++){
        myTomeo->setSeasonNumber(i);

        //cout << i << endl;

        ScrollWindow * scrollWin = new ScrollWindow(myTomeo);
        QScrollArea *scroll = new QScrollArea(scrollWin);

        MasterLayout *eSeason = new DseasonsScroll(myTomeo);
        eSeason->setGeometry(QRect(0,50,1000,300));
        eSeason->createWidgets();

        QWidget* content = new QWidget();
        scroll->setWidget(content);
        content->setLayout(eSeason);
        

        addWidget(scrollWin);
    }

    //myTomeo->setSeasonNumber(1);
}


