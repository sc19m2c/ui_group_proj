//
// Created by sc18mmyc on 12/12/2019.
//

#ifndef COURSEWORK2_UI_DSEASONSSCROLL_H
#define COURSEWORK2_UI_DSEASONSSCROLL_H

#include "masterLayout.h"

class DseasonsScroll: public MasterLayout {
public:
    DseasonsScroll(MyTomeo *tom):MasterLayout(tom){}
    void setGeometry(const QRect &rect);
    void createWidgets();
    void calcIconSize(QIcon);
};


#endif //COURSEWORK2_UI_DSEASONSSCROLL_H
