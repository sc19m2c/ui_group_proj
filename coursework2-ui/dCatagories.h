//
// Created by sc19m2c on 06/12/19.
//

#ifndef COURSEWORK2_UI_DERIVED_H
#define COURSEWORK2_UI_DERIVED_H
#include "masterLayout.h"

class Dcatagories : public MasterLayout {
public:
    Dcatagories(MyTomeo *tom):MasterLayout(tom){}
    void setGeometry(const QRect &rect);
    void createWidgets();

};


#endif //COURSEWORK2_UI_DERIVED_H
