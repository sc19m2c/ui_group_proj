//
// Created by sc19m2c on 04/12/19.
//

#ifndef COURSEWORK2_UI_READCSV_H
#define COURSEWORK2_UI_READCSV_H
#include <string>
#include <vector>

using namespace std;

class ReadCsv {
        string fileName;
        string delimeter;

    public:

        ReadCsv(vector<string> keys, vector<vector<string>> data);

        ReadCsv(string filename, string delm = ",") :
                fileName(filename), delimeter(delm)
        { }

        // Function to fetch data from a CSV File
        void loadData();



        ReadCsv *getById(string id);
        ReadCsv *getAllWhere(string field, string value);
        int indexOf(string keyValue);
        vector<string> getDataField(string colName);
        vector<vector<string>> getData();
        void toString();



    private:
        vector<vector<string>> data;
        vector<string> keys;
};


#endif //COURSEWORK2_UI_READCSV_H
