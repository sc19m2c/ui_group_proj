//
// Created by sc19m2c on 06/12/19.
//

#ifndef COURSEWORK2_UI_DPLAYERFULL_H
#define COURSEWORK2_UI_DPLAYERFULL_H
#include "masterLayout.h"
#include "VideoPlayer.h"
#include "readCsv.h"

class DplayerFull : public MasterLayout {
Q_OBJECT
public:
    DplayerFull(MyTomeo *tom):MasterLayout(tom){};
    void setGeometry(const QRect &rect);
    void createWidgets();

private:
    VideoPlayer *videoPlayer;
    QPushButton *nextButton;
    QPushButton *previousButton;
    string video_path;
    int episode = 0;
    ReadCsv *tvShows;

private slots:
    void next();
    void previous();
};


#endif //COURSEWORK2_UI_DPLAYERFULL_H
