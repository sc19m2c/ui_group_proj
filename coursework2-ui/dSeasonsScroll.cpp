//
// Created by sc18mmyc on 12/12/2019.
//
#include <QtWidgets/QPushButton>
#include <iostream>
#include <QtWidgets/QLabel>
#include "dSeasonsScroll.h"
#include "readCsv.h"
#include "dNavbar.h"


using namespace std;


void DseasonsScroll::calcIconSize(QIcon icon){
    //cout << icon.pixmap() << endl;
}


void DseasonsScroll::setGeometry(const QRect &r) {
    QLayout::setGeometry(r);

    int mx = r.width() * 0.2;
    int margin = mx / 4;
    //int maxWidth = (r.width()-mx)/3;
    int maxWidth = 200;
    int labelHeight = 40;
    int marginVert = margin / 2;
    for (int i = 0; i < list_.size(); i++) {
        QLabel *label = dynamic_cast<QLabel *>(list_.at(i)->widget());
        if (label) {
            label->setGeometry(QRect(r.x() - 55 + 100 * i, r.y() + 105, 150, 50));
            continue;
        }

        //label->setStyleSheet("background-color:yellow;");
        QPushButton *push = dynamic_cast<QPushButton *>(list_.at(i)->widget());
        if (push) {
            QSize *icon = new QSize(130, 100);
            push->setIconSize(QSize(130, 100));
            push->setGeometry(QRect(r.x() + 100 * i, r.y(), 130, 100));
            push->setStyleSheet("background-color:white;"
                                "border: 0px;");
            continue;
        }
    }
    //myTomeo->setCatPageHeight(r.y()+(row+1)*maxWidth+(row)*labelHeight+row*marginVert+labelHeight);
}

void DseasonsScroll::createWidgets(){
    QLabel *background = new QLabel();
    addWidget(background);

    std::string tvShow_id = getTomeo()->getTvShow_id();
    char const *tvid = tvShow_id.c_str();

    ReadCsv *allShows = new ReadCsv("/home/csunix/sc18mmyc/Downloads/ui_group_proj/coursework2-ui/tmp/allTvShows.csv");
    allShows->loadData();

    ReadCsv *series = allShows->getAllWhere("tvShow_id", tvid);

    std::string s = std::to_string(myTomeo->getSeasonNumber());
    char const *szn = s.c_str();
    ReadCsv *seas = series->getAllWhere("season", szn);
    QLabel *slabel = new QLabel(QString::fromStdString("Season " + s));
    slabel->setGeometry(20,75, 100, 100);
    addWidget(slabel);
    for(vector<string> eps : seas->getData()){
        string episode = eps[series->indexOf("episode")];
        string path = eps[series->indexOf("coverImg")];
        string season = eps[series->indexOf("season")];
        QPushButton *push = new QPushButton();
        QIcon ButtonIcon(QPixmap(QString::fromStdString(path)));
        push->setIcon(ButtonIcon);
        connect(push, &QPushButton::clicked, [=] {
            clickTvShow("SeasonsView", eps[series->indexOf("video_path")]);
        });
        addWidget(push);

        QLabel *label = new QLabel(QString::fromStdString("Episode " + episode));
        addWidget(label);

    }

}