//
// Created by sc19m2c on 28/11/19.
//

#ifndef COURSEWORK2_UI_MYTOMEO_H
#define COURSEWORK2_UI_MYTOMEO_H
#include <QWidget>
#include <string>
using namespace std;
class MyTomeo : public QWidget{
Q_OBJECT
    public:
        MyTomeo();
        void setPageName(string);
        string getPageName();
        void setTvShow_id(string id);
        string getTvShow_id();
        void setVideoDisplay_path(string id);
        string getVideoDisplay_path();

        void changePage(string nextPage);
        string getPreviousPage();
        string getNextPage();

        int getCatPageHeight();
        void setCatPageHeight(int h);

    private:
        void deleteLayout(QLayout *layout);
        void createWidgets();
        string pageName;
        QColor colour;
        vector<string> pages = {"Settings", "Catagories", "SeasonsView", "PlayerFull"};
        string tvShow_id;
        string videoDisplay_path;
        int catPageHeight;
};



#endif //COURSEWORK2_UI_MYTOMEO_H
