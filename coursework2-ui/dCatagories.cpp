//
// Created by sc19m2c on 06/12/19.
//

#include "dCatagories.h"
#include <iostream>
#include <QtWidgets/QScrollArea>
#include "QPushButton"
#include "readCsv.h"
#include "dNavbar.h"
#include "scrollWindow.h"
#include "dCatScroll.h"

using namespace std;


// renders the scroll window and sets the dinemsion

void setScroller(ScrollWindow *container,int MAX_RES_WIDTH, int usedHeight, int footerSize, const QRect &r, MyTomeo *myTomeo){

    //defines the geormetry dependig on the width of the page
    if (r.width()> MAX_RES_WIDTH){
        container->setGeometry(QRect((r.width()-MAX_RES_WIDTH)/2,usedHeight,MAX_RES_WIDTH,r.height()-footerSize));
    }else{
        container->setGeometry(QRect(0,0+usedHeight,r.width(),r.height()-footerSize));
    }

    container->show();
    QList<QObject*> inside = container->children();

    // loop threough scroll windows children to find the scroll area
    for (int i = 0 ; i < inside.length() ; i ++){
        QScrollArea* scroll = dynamic_cast<QScrollArea*>(inside[i]);
        if (scroll){
            scroll->widget()->setMinimumSize(r.width()-20, myTomeo->getCatPageHeight()); //// need a function to define the height

            scroll->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
            scroll->setWidgetResizable( true );

            scroll->setMinimumSize(r.width()<MAX_RES_WIDTH? r.width(): MAX_RES_WIDTH, r.height()-usedHeight-footerSize);
            if (r.width()> MAX_RES_WIDTH){
                scroll->setGeometry(QRect(0,0,container->sizeHint().width(),r.height()-usedHeight-footerSize));
            }else{
                scroll->setGeometry(QRect(0,0,container->sizeHint().width(),r.height()-usedHeight-footerSize));
            }
            continue;
        }
    }
}

void Dcatagories::setGeometry(const QRect &r){
    QLayout::setGeometry(r);

    // for all the Widgets added in ResponsiveWindow.cpp

    for (int i = 0; i < list_.size(); i++) {

        QLayoutItem *o = list_.at(i);

        MasterLayout* nav = dynamic_cast<Dnavbar*>(list_.at(i));
        if (nav){
            nav->setGeometry(QRect(r.x(),r.y(),r.width(),50));
            continue;
        }


        ScrollWindow* container = dynamic_cast<ScrollWindow*>(list_.at(i)->widget());
        if(container){
            setScroller(container,r.width(), 50, 0, r, myTomeo);
            continue;
        }

        try {
        }
        catch (bad_cast) {
            cout << "warning, unknown widget class iQQQQQn layout" << i << endl;
        }
    }
}



void Dcatagories::createWidgets(){

    MasterLayout *nav = new Dnavbar(myTomeo);
    nav->createWidgets();
    addItem(nav);

    ScrollWindow * scrollWin = new ScrollWindow(myTomeo);
    QScrollArea *scroll = new QScrollArea(scrollWin);
    QWidget* content = new QWidget();
    scroll->setWidget(content);
    MasterLayout *cat = new DcatScroll(myTomeo);
    cat->createWidgets();
    content->setLayout(cat);

    addWidget(scrollWin);

}