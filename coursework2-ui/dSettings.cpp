//
// Created by sc19m2c on 06/12/19.
//

#include "dSettings.h"
#include <iostream>
#include <QColor>
#include "QPushButton"
#include "readCsv.h"
#include "dNavbar.h"
using namespace std;

Dsettings::Dsettings(MyTomeo *tom):MasterLayout(tom) {
    colours.insert(pair<string, QColor>("Pink", QColor(204,0,204)));
    colours.insert(pair<string, QColor>("Red", QColor(204,0,0)));
    colours.insert(pair<string, QColor>("Blue", QColor(0,255,255)));
    colours.insert(pair<string, QColor>("Yellow", QColor(255,255,0)));
}

void Dsettings::setGeometry(const QRect &r){
    QLayout::setGeometry(r);

    // for all the Widgets added in ResponsiveWindow.cpp

    for (int i = 0; i < list_.size(); i++) {

        QLayoutItem *o = list_.at(i);

        MasterLayout* nav = dynamic_cast<Dnavbar*>(list_.at(i));
        if (nav){
            nav->setGeometry(QRect(r.x(),r.y(),r.width(),50));
            continue;
        }

        QPushButton* push = dynamic_cast<QPushButton*>(list_.at(i)->widget());
        if (push){
            QRegion region(QRect(r.x(),r.y(),200,200), QRegion::Ellipse);
            push->setMask(region);
            push->setIconSize(QSize(300,300));

            push->setGeometry(QRect(r.x()+20,r.y()+20+(i*200),200,200));
            continue;
        }

        try {
        }
        catch (bad_cast) {
            cout << "warning, unknown widget class iQQQQQn layout" << i << endl;
        }
    }
}

void Dsettings::createWidgets(){

    MasterLayout *nav = new Dnavbar(myTomeo);
    nav->createWidgets();
    addItem(nav);
}