//
// Created by sc19m2c on 04/12/19.
//

#include "readCsv.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <string>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <X11/X.h>

ReadCsv::ReadCsv(vector<string> keysIn, vector<vector<string>> dataIn) {
    vector<vector<string>> *output = new vector<vector<string>>();
    for(vector<string> d :dataIn){
        output->push_back(d);
    }
    data = *output;
    keys = keysIn;

}

void ReadCsv::loadData()
{
    std::ifstream file(fileName);

    std::vector<std::vector<std::string> > dataList;

    std::string line = "";
    // Iterate through each line and split the content using delimeter
    bool first = true;
    while (getline(file, line)) {
        std::vector<std::string> vec;
        boost::algorithm::split(vec, line, boost::is_any_of(delimeter));

        if(first){
            keys = vec;
        }else{
            dataList.push_back(vec);
        }
        first = false;

    }
    // Close the File
    file.close();

    data = dataList;
    return;
}


ReadCsv *ReadCsv::getById(string id){
    vector<vector<string>> *output = new vector<vector<string>>();
    for(vector<string> vec : data){
        if(vec[0]==id) {
            output->push_back(vec);
            ReadCsv *read = new ReadCsv(keys,*output);
            return read;
        }
    }
}


///////errror not resaponding as expected
ReadCsv *ReadCsv::getAllWhere(string field, string value){
    vector<vector<string>> *output = new vector<vector<string>>();
    int index = indexOf(field);
    for(vector<string> vec : data){
        if(vec[index] == value) {
            output->push_back(vec);
        }
    }
    ReadCsv *read = new ReadCsv(keys,*output);
    return read;
}


vector<vector<string>> ReadCsv::getData() {
    return data;
}


int ReadCsv::indexOf(string keyValue){
    vector<string>::iterator it = find(keys.begin(), keys.end(), keyValue);
    if( it != keys.end()){
        return distance(keys.begin(), it);
    }else{
        throw "Field not found";
    }
}


vector<string> ReadCsv::getDataField(string colName){
    int index = indexOf(colName);
    vector<string> *output = new vector<string>();
    for (vector<string> row: data){
        output->push_back(row[index]);
    }
    return *output;
}

void ReadCsv::toString(){
    for (vector<string> vec : data){
        int i = 0;
        for(string data: vec){
            cout << keys[i] << " = " << data << " , " ;
            i++;
        }
        cout << endl;
    }
}