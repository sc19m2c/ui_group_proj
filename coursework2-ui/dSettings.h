//
// Created by sc19m2c on 06/12/19.
//

#ifndef COURSEWORK2_UI_DSETTINGS_H
#define COURSEWORK2_UI_DSETTINGS_H
#include "masterLayout.h"

class Dsettings : public MasterLayout {
public:
    Dsettings(MyTomeo *tom);
    void setGeometry(const QRect &rect);
    void createWidgets();


private:
    map<string,QColor> colours;
    map<string,string> icons;

private slots:
    void changeIcon(string iconPath);
    void changeColor(string colour);
};


#endif //COURSEWORK2_UI_DSETTINGS_H
