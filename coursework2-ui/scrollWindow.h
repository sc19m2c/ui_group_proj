//
// Created by sc19m2c on 11/12/19.
//

#ifndef COURSEWORK2_UI_SCROLLWINDOW_H
#define COURSEWORK2_UI_SCROLLWINDOW_H


#include <QtCore/QRect>
#include <QtWidgets/QWidget>
#include "myTomeo.h"
#include "dCatScroll.h"

class ScrollWindow: public QWidget {

public:
    ScrollWindow(MyTomeo *tom);
    MyTomeo *getTomeo();
    void createWidgets(const QRect &r);
    MasterLayout *getCatScroll();

private:

    MyTomeo *myTomeo;
    MasterLayout *scrollLayout;
};

#endif //COURSEWORK2_UI_SCROLLWINDOW_H
