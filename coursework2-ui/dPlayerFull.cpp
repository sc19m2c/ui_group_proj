//
// Created by sc19m2c on 06/12/19.
//
#include <iostream>
#include <QtWidgets/QPushButton>
#include "dPlayerFull.h"
#include "dNavbar.h"
#include <sstream>

using namespace std;

void DplayerFull::setGeometry(const QRect &r){
    QLayout::setGeometry(r);

    // place all the widgets on the right place in the layout

    for (int i = 0; i < list_.size(); i++) {

        QLayoutItem *o = list_.at(i);

        VideoPlayer* videoPlayer = dynamic_cast<VideoPlayer*>(list_.at(i)->widget());
        if (videoPlayer){
            videoPlayer->setGeometry(QRect(r.x(),r.y(),r.width(),r.height()));
            continue;
        }
        MasterLayout* nav = dynamic_cast<Dnavbar*>(list_.at(i));
        if (nav) {
            nav->setGeometry(QRect(r.x(), r.y(), r.width(), 50));
            continue;
        }
        //when you're on the last episode make sure you can't click on the next button
        if(video_path.find("episode5") == std::string::npos)
            nextButton->setGeometry(QRect(r.x() + r.width() - 50, r.y() + r.height() / 2 - 50, 50, 100));
        else
            nextButton->setGeometry(QRect(r.x() - 200, r.y() + - 200, 0, 0));
        //when you're on the first episode make sure you can't click on the previous button
        if(video_path.find("episode1") == std::string::npos)
            previousButton->setGeometry(QRect(r.x(),r.y()+r.height()/2-50,50,100));
        else
            previousButton->setGeometry(QRect(r.x() - 200, r.y() + - 200, 0, 0));
    }
}

void DplayerFull::createWidgets(){
    //find out which episode needs to be played and loads all other episodes through csv.
    video_path = getTomeo()->getVideoDisplay_path();
    tvShows = new ReadCsv("/home/csunix/sc19mvdl/CLionProjects/git_ui_cw2/ui_group_proj/coursework2-ui/tmp/allTvShows.csv");
    tvShows->loadData();
    ReadCsv *line = tvShows->getAllWhere("video_path",getTomeo()->getVideoDisplay_path());
    string id = line->getDataField("id")[0];
    stringstream geek(id);
    geek >> episode;

    //create the videoplayer and gives the right url to play
    QUrl url(QString::fromStdString(video_path));
    videoPlayer = new VideoPlayer();
    videoPlayer->setUrl(url);
    addWidget(videoPlayer);
    videoPlayer->play();

    //create the next button so you can go to the next episode in thIS season from this show
    nextButton = new QPushButton;
    QPixmap pixmapNext("/home/csunix/sc19mvdl/CLionProjects/git_ui_cw2/ui_group_proj/coursework2-ui/tmp/next.png");
    QIcon ButtonNext(pixmapNext);
    nextButton->setIcon(ButtonNext);
    nextButton->setIconSize(QSize(50, 100));
    connect(nextButton, &QPushButton::clicked, [=] {
        next();
    });
    addWidget(nextButton);


    //create the next button so you can go to the next episode in thIS season from this show
    previousButton = new QPushButton;
    QPixmap pixmapBack("/home/csunix/sc19mvdl/CLionProjects/git_ui_cw2/ui_group_proj/coursework2-ui/tmp/back.png");
    QIcon ButtonBack(pixmapBack);
    previousButton->setIcon(ButtonBack);
    previousButton->setIconSize(QSize(50, 100));
    addWidget(previousButton);
    connect(previousButton, &QPushButton::clicked, [=] {
        previous();
    });

    //create the masterlayout so you can go back and forth between screens
    MasterLayout *nav = new Dnavbar(myTomeo);
    nav->createWidgets();
    addItem(nav);
}

//find out what the next episode is and play this in the videoplayer
void DplayerFull::next()
{
    video_path=tvShows->getDataField("video_path")[episode];
    episode++;
    QUrl url(QString::fromStdString(video_path));
    videoPlayer->setUrl(url);
    videoPlayer->play();
}

//find out what the previous episode is and play this in the videoplayer
void DplayerFull::previous()
{
    video_path=tvShows->getDataField("video_path")[episode-2];
    episode--;
    QUrl url(QString::fromStdString(video_path));
    videoPlayer->setUrl(url);
    videoPlayer->play();
}


