#include "VideoPlayer.h"
#include "VideoLayout.h"


#include <QtWidgets>
#include <QVideoWidget>

VideoPlayer::VideoPlayer(QWidget *parent)
        : QWidget(parent)
{
    //make a screen where the video will be played
    m_mediaPlayer = new QMediaPlayer(this, QMediaPlayer::VideoSurface);
    QVideoWidget *videoWidget = new QVideoWidget;

    //make the play/pause button so the video can be paused
    m_playButton = new QPushButton;
    m_playButton->setEnabled(false);
    m_playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    connect(m_playButton, &QAbstractButton::clicked, this, &VideoPlayer::play);

    //make the slider at the bottom so you can go to any moment in the video
    m_positionSlider = new QSlider(Qt::Horizontal);
    m_positionSlider->setRange(0, 0);
    connect(m_positionSlider, &QAbstractSlider::sliderMoved, this, &VideoPlayer::setPosition);

    //make the mute button so the music can be muted
    muteButton = new QToolButton;
    muteButton->setIcon(style()->standardIcon(QStyle::SP_MediaVolume));
    playerMuted=false;
    connect(muteButton, &QAbstractButton::clicked, this, &VideoPlayer::setMuted);

    //make the volumeslider so the volume can be turned up or down
    m_volumeSlider = new QSlider(Qt::Horizontal);
    m_volumeSlider->setRange(0, 100);
    volume=50;
    m_volumeSlider->setSliderPosition(volume);
    connect(m_volumeSlider, &QSlider::sliderMoved, this, &VideoPlayer::changeVolume);

    VideoLayout *layout = new VideoLayout();
    layout->addWidget(videoWidget);
    layout->addWidget(m_playButton);
    layout->addWidget(m_positionSlider);
    layout->addWidget(muteButton);
    layout->addWidget(m_volumeSlider);
    setLayout(layout);

    m_mediaPlayer->setVideoOutput(videoWidget);
    connect(m_mediaPlayer, &QMediaPlayer::stateChanged, this, &VideoPlayer::mediaStateChanged);
    connect(m_mediaPlayer, &QMediaPlayer::positionChanged, this, &VideoPlayer::positionChanged);
    connect(m_mediaPlayer, &QMediaPlayer::durationChanged, this, &VideoPlayer::durationChanged);
    play();
}

VideoPlayer::~VideoPlayer()
{
}


//let the video play the url that is given
void VideoPlayer::setUrl(const QUrl &url)
{
    setWindowFilePath(url.isLocalFile() ? url.toLocalFile() : QString());
    m_mediaPlayer->setMedia(url);
    m_playButton->setEnabled(true);
}

//change the state to either play or paused based on current state
void VideoPlayer::play()
{
    switch (m_mediaPlayer->state()) {
        case QMediaPlayer::PlayingState:
            m_mediaPlayer->pause();
            break;
        default:
            m_mediaPlayer->play();
            break;
    }
}

//change the icon of the play/pause button based on current state
void VideoPlayer::mediaStateChanged(QMediaPlayer::State state)
{
    switch(state) {
        case QMediaPlayer::PlayingState:
            m_playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPause));
            break;
        default:
            m_playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
            break;
    }
}

//when the position in the mediaplayer, change it on the slider as well
void VideoPlayer::positionChanged(qint64 position)
{
    m_positionSlider->setValue(position);
}

//when the duration of video is changed (for example by clicking a new video) also change the range of the slider
void VideoPlayer::durationChanged(qint64 duration)
{
    m_positionSlider->setRange(0, duration);
}

//if the position is changed on the slider let the video go to that moment
void VideoPlayer::setPosition(int position)
{
    m_mediaPlayer->setPosition(position);
}

//change icon of the mute button and change the slider to make it 0
void VideoPlayer::setMuted()
{
    if (playerMuted) {
        playerMuted = false;
        muteButton->setIcon(style()->standardIcon(QStyle::SP_MediaVolume));
        m_volumeSlider->setSliderPosition(volume);
    }
    else{
        playerMuted = true;
        muteButton->setIcon(style()->standardIcon(QStyle::SP_MediaVolumeMuted));
        m_volumeSlider->setSliderPosition(0);
    }
}

//when the volumeslider is on 0 mute the volume and change the icon. Otherwise update the volume variable
void VideoPlayer::changeVolume()
{
    if(playerMuted&&volume>0){
        playerMuted = false;
        muteButton->setIcon(style()->standardIcon(QStyle::SP_MediaVolume));
        volume=m_volumeSlider->sliderPosition();
    }
    else if(!playerMuted&&m_volumeSlider->sliderPosition()==0){
        playerMuted=true;
        muteButton->setIcon(style()->standardIcon(QStyle::SP_MediaVolumeMuted));
    }
    else
        volume=m_volumeSlider->sliderPosition();
}




