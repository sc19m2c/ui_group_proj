//
// Created by sc19m2c on 11/12/19.
//

#ifndef COURSEWORK2_UI_DCATSCROLL_H
#define COURSEWORK2_UI_DCATSCROLL_H


#include "masterLayout.h"

class DcatScroll: public MasterLayout {
public:
    DcatScroll(MyTomeo *tom):MasterLayout(tom){}
    void setGeometry(const QRect &rect);
    void createWidgets();

};


#endif //COURSEWORK2_UI_DCATSCROLL_H
