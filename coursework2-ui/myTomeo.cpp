//
// Created by sc19m2c on 28/11/19.
//

#include "myTomeo.h"
//
// Created by twak on 14/10/2019.
//
#include <QString>
#include <iostream>
#include <vector>
#include <QApplication>
#include <string>
#include "readCsv.h"
#include "dCatagories.h"
#include "masterLayout.h"
#include "dSeasonsView.h"
#include "dPlayerFull.h"
#include "dSettings.h"
#include "scrollWindow.h"

using namespace std;

MyTomeo::MyTomeo() {
    colour = QColor(0,255,255);
    setWindowTitle("2811: Coursework 2");
    setMinimumSize(519, 519); /// minimus size for our screen - ipad mini --- need to get mini size of ipad mini
    setMaximumSize(924, 924); /// needs complex logic to ensure that the raion doesnt exceed 2048/2732
    setGeometry(100,50,692,924); ///
    setPageName("Catagories");
    createWidgets();
}

//// the page needs to change as a variabel of my temoe not mai layout 

void MyTomeo::createWidgets() {


    QPalette pal = QPalette();
    pal.setColor(QPalette::Background, colour);
    setAutoFillBackground(true);
    setPalette(pal);

    if(pageName == "Settings"){
        cout << "getting rendered cat" << endl;
        MasterLayout *settings = new Dsettings(this);
        settings->createWidgets();

        setLayout(settings);
    }
    else if(pageName == "Catagories"){
        cout << "getting rendered cat" << endl;
        //ScrollWindow * scrollWin = new ScrollWindow(this);
        MasterLayout *cat = new Dcatagories(this);
        cat->createWidgets();
        setLayout(cat);
    }
    else if (pageName == "SeasonsView"){
        cout << "getting rendered player" << endl;
        MasterLayout *seasons = new DseasonsView(this);
        seasons->createWidgets();
        setLayout(seasons);
    }
    else if (pageName == "PlayerFull"){
        cout << "getting rendered player" << endl;
        MasterLayout *player = new DplayerFull(this);
        player->createWidgets();
        setLayout(player);
    }


}




void MyTomeo::changePage(string nextPage){
    cout << "passed the funtion" << endl;
    deleteLayout(layout());
    cout << " setting page to " << nextPage << endl;
    setPageName(nextPage);
    createWidgets();
}


void MyTomeo::deleteLayout(QLayout *layout){
    cout << "deleting layout" << endl;
    QLayoutItem * item;
    QLayout * sublayout;
    QWidget * widget;
    while ((item = layout->takeAt(0))) {
        if ((sublayout = item->layout()) != 0) {
            deleteLayout(sublayout);
        }
        else if ((widget = item->widget()) != 0) {widget->hide(); delete widget;}
        else {delete item;}
    }
    delete layout;
}




string MyTomeo::getPreviousPage(){
    vector<string>::iterator it = std::find(pages.begin(), pages.end(), pageName);
    if( it != pages.end()){
        long index = distance(pages.begin(), it);
        if(index != 0 ){
            return pages[index-1];
        }
    }else{
        throw "Field not found";
    }
}

string MyTomeo::getNextPage(){
    vector<string>::iterator it = std::find(pages.begin(), pages.end(), pageName);
    if( it != pages.end()){
        long index = distance(pages.begin(), it);
        if(index != pages.size()-1 ){
            return pages[index+1];
        }
    }else{
        throw "Field not found";
    }

}

void MyTomeo::setPageName(std::string name){
    pageName = name;
}

string MyTomeo::getPageName(){
    return pageName;
}

void MyTomeo::setTvShow_id(string tv_id){
    tvShow_id = tv_id;
}

string MyTomeo::getTvShow_id(){
    return tvShow_id;
}

void MyTomeo::setVideoDisplay_path(string vid_path){
    videoDisplay_path = vid_path;
}

string MyTomeo::getVideoDisplay_path(){
    return videoDisplay_path;
}

int MyTomeo::getCatPageHeight(){
    return catPageHeight;
}

void MyTomeo::setCatPageHeight(int h){
    catPageHeight =h;
}

// please DO NOT edit this file beyond this line
int main(int argc, char *argv[]) {


    // let's just check that Qt is operational first
    cout << "Qt version: " << QT_VERSION_STR << endl;

    // create the Qt Application
    QApplication app(argc, argv);


    // interactive mode - run the regular superclass
    MyTomeo window;
    window.show();
    // wait for the app to terminate
    return app.exec();


}

