//
// Created by sc19m2c on 06/12/19.
//

#ifndef COURSEWORK2_UI_DSEASONSVIEW_H
#define COURSEWORK2_UI_DSEASONSVIEW_H
#include "masterLayout.h"


class DseasonsView : public MasterLayout {
public:
    DseasonsView(MyTomeo *tom):MasterLayout(tom){};
    void setGeometry(const QRect &rect);
    void createWidgets();

};

#endif //COURSEWORK2_UI_DSEASONSVIEW_H
