//
// Created by sc19m2c on 11/12/19.
//

#include "dNavbar.h"
#include "dCatagories.h"
#include <iostream>
#include "QPushButton"
#include "readCsv.h"

using namespace std;




void Dnavbar::setGeometry(const QRect &r){

    QLayout::setGeometry(r);
    cout << "settijng nav bar" << endl;
    for (int i = 0; i < list_.size(); i++) {

        QLayoutItem *o = list_.at(i);

        QPushButton* backS = dynamic_cast<QPushButton*>(list_.at(i)->widget());
        if (backS){
            if(backS->text() == "Back" || backS->text() == "Settings"){
                backS->setGeometry(QRect(r.x()+10,r.y(),70,r.height()));
            }
            else if(backS->text() == "Cancle"){
                backS->setGeometry(QRect(r.x()+10,r.y(),70,r.height()));
            }
            else if(backS->text() == "Save"){
                backS->setGeometry(QRect(r.x()+r.width()-80,r.y(),70,r.height()));
            }
            continue;
        }

        try {
        }
        catch (bad_cast) {
            cout << "warning, unknown widget class iQQQQQn layout" << i << endl;
        }
    }
}



void Dnavbar::createWidgets(){



    if (myTomeo->getPageName() == "Settings"){
        QPushButton *save = new QPushButton("Save");
        save->setStyleSheet("color: blue; background-color: yellow");
        connect(save, &QPushButton::clicked, [=]{
            backPage(myTomeo->getNextPage());
        });
        addWidget(save);


        QPushButton *cancle = new QPushButton("Cancle");
        cout << "herereree" << cancle->text().toStdString() << endl;
        cancle->setStyleSheet("color: blue; background-color: yellow");
        connect(cancle, &QPushButton::clicked, [=]{
            backPage(myTomeo->getNextPage());
        });
        addWidget(cancle);
    }else{
        string prevPage =myTomeo->getPreviousPage();
        QString title = prevPage == "Settings"?"Settings":"Back";
        QPushButton *backS = new QPushButton(title);
        backS->setStyleSheet("color: blue; background-color: yellow");
        connect(backS, &QPushButton::clicked, [=]{
            backPage(prevPage);
        });
        addWidget(backS);
    }



}