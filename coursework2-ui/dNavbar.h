//
// Created by sc19m2c on 11/12/19.
//

#ifndef COURSEWORK2_UI_DNAVBAR_H
#define COURSEWORK2_UI_DNAVBAR_H
#include "masterLayout.h"

class Dnavbar : public MasterLayout {
public:
    Dnavbar(MyTomeo *tom):MasterLayout(tom){}
    void setGeometry(const QRect &rect);
    void createWidgets();

};



#endif //COURSEWORK2_UI_DNAVBAR_H
