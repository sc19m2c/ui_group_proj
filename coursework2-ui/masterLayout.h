//
// Created by sc19m2c on 06/12/19.
//

#ifndef COURSEWORK2_UI_MASTERLAYOUT_H
#define COURSEWORK2_UI_MASTERLAYOUT_H



#include <QApplication>
#include <QMediaPlayer>
#include <vector>
#include <QTimer>
#include <QLayout>
#include <string>
#include "myTomeo.h"


using namespace std;


class MasterLayout : public QLayout {
Q_OBJECT
public:
    MasterLayout(MyTomeo *tom);
    ~MasterLayout();

    // standard functions for a QLayout
    virtual void setGeometry(const QRect &rect);
    virtual void createWidgets();

    void addItem(QLayoutItem *item);
    QSize sizeHint() const;
    QSize minimumSize() const;
    int count() const;
    QLayoutItem *itemAt(int) const;
    QLayoutItem *takeAt(int);

    MyTomeo *getTomeo();
    //DcomponentNav *getNavbar();

protected:
    QList<QLayoutItem*> list_;
    MyTomeo *myTomeo;
    //DcomponentNav *navBar;

protected slots:
    void clickTvShow(string pagename, string data);
    void backPage(string pagename);

};

#endif //COURSEWORK2_UI_MASTERLAYOUT_H
