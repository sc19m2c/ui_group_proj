#ifndef VIDEOPLAYER_H
#define VIDEOPLAYER_H

#include <QMediaPlayer>
#include <QWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QToolButton>

class QAbstractButton;
class QSlider;
class QLabel;
class QUrl;

class VideoPlayer : public QWidget
{
Q_OBJECT



public:
    VideoPlayer(QWidget *parent = nullptr);
    ~VideoPlayer();
    void setUrl(const QUrl &url);


public slots:
    void play();
    void setPosition(int position);
    void setMuted();
    void changeVolume();

public slots:
    void mediaStateChanged(QMediaPlayer::State state);
    void positionChanged(qint64 position);
    void durationChanged(qint64 duration);


private:
    QMediaPlayer* m_mediaPlayer;
    QAbstractButton *m_playButton;
    QSlider *m_positionSlider;
    QToolButton *muteButton;
    bool playerMuted;
    QSlider *m_volumeSlider;
    int volume;
};

#endif
