//
// Created by sc19mvdl on 12/12/19.
//

#ifndef COURSEWORK2_UI_VIDEOLAYOUT_H
#define COURSEWORK2_UI_VIDEOLAYOUT_H


#include <QtGui>
#include <QList>
#include <QLayout>
#include <string>
#include <QPushButton>
#include <QtWidgets/QLayoutItem>

using namespace std;

class VideoLayout : public QLayout
{
Q_OBJECT
public:
    VideoLayout():QLayout(){};
    ~VideoLayout();

    void setGeometry(const QRect &rect);


    void addItem(QLayoutItem *item);
    QSize sizeHint() const;
    QSize minimumSize() const;
    int count() const;
    QLayoutItem *itemAt(int) const;
    QLayoutItem *takeAt(int);

private:
    QList<QLayoutItem*> list_;
};


#endif //COURSEWORK2_UI_VIDEOLAYOUT_H
