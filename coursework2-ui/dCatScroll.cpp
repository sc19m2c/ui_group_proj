//
// Created by sc19m2c on 11/12/19.
//

#include <QtWidgets/QPushButton>
#include <iostream>
#include <QtWidgets/QLabel>
#include "dCatScroll.h"
#include "readCsv.h"
#include "dNavbar.h"


using namespace std;


void calcIconSize(QIcon icon){
    //cout << icon.pixmap() << endl;
}


void DcatScroll::setGeometry(const QRect &r){
    QLayout::setGeometry(r);

    int mx = r.width()*0.2;
    int margin = mx/4;
    int maxWidth = (r.width()-mx)/3;
    int labelHeight = 40;
    int marginVert = margin/2;
    // for all the Widgets added in ResponsiveWindow.cpp
    int y =0;
    int row =0;
    int count =1;
    for (int i = 0; i < list_.size(); i++) {
        if (y<3){
            if(count ==1){
                QLabel *label = dynamic_cast<QLabel*>(list_.at(i)->widget());

                label->setGeometry(QRect(r.x()+(y+1)*margin+y*maxWidth-5
                        ,r.y()+row*maxWidth+row*labelHeight+row*marginVert-5
                        ,maxWidth+10
                        ,maxWidth+10));
                label->setStyleSheet(
                        "background-color:white;"
                );
                QRegion region(QRect(r.x(),r.y(),maxWidth+10,maxWidth+10), QRegion::Ellipse);
                label->setMask(region);
                count ++;
            }else if (count ==2){
                QPushButton* push = dynamic_cast<QPushButton*>(list_.at(i)->widget());
                push->setGeometry(QRect(r.x()+(y+1)*margin+y*maxWidth
                        ,r.y()+row*maxWidth+row*labelHeight+row*marginVert
                        ,maxWidth
                        ,maxWidth));

                QRegion region(QRect(r.x(),r.y(),maxWidth,maxWidth), QRegion::Ellipse);
                push->setMask(region);
                push->setIconSize(QSize(maxWidth,maxWidth));
                calcIconSize(push->icon());
                count ++;
            }
            else if (count ==3){
                QLabel *label = dynamic_cast<QLabel*>(list_.at(i)->widget());
                label->setGeometry(QRect(r.x()+(y+1)*margin+y*maxWidth
                        ,r.y()+(row+1)*maxWidth+(row)*labelHeight+row*marginVert
                        ,maxWidth
                        ,labelHeight));
                label->setAlignment(Qt::AlignCenter);
                y++;
                count = 1;
                if (y == 3) {
                    y = 0;
                    row++;
                }
            }
        }
    }
    myTomeo->setCatPageHeight(r.y()+(row+1)*maxWidth+(row)*labelHeight+row*marginVert+labelHeight);
}



void DcatScroll::createWidgets(){
    ReadCsv *tvShows = new ReadCsv("/home/csunix/sc19m2c/CLionProjects/coursework2-ui-git/ui_group_proj/coursework2-ui/tmp/TvShows.csv");
    tvShows->loadData();



    for (vector<string> row : tvShows->getData()){
        QLabel *background = new QLabel();
        addWidget(background);

        // create the button for each catagory of tv show
        QPushButton *push = new QPushButton();
        push->setStyleSheet("color: blue; background-color: yellow");
        // set icon for each catagory of tv show

        string path = row[tvShows->indexOf("coverImg")];
        QPixmap pixmap(QString::fromStdString(path));
        QIcon ButtonIcon(pixmap);
        push->setIcon(ButtonIcon);

        // connect the function to change page
        connect(push, &QPushButton::clicked, [=]{
            clickTvShow("SeasonsView", row[tvShows->indexOf("id")]);
        });
        //add widget to layout
        addWidget(push);

        // create label for the tv show
        QLabel *label = new QLabel(QString::fromStdString(row[tvShows->indexOf("tvShowName")]));
        addWidget(label);

    }

}