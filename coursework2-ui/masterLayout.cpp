//
// Created by sc19m2c on 06/12/19.
//

#include "masterLayout.h"
//
// Created by sc19m2c on 28/11/19.
//


//
// Created by sc19m2c on 28/11/19.
//

#include "readCsv.h"
//
// Created by twak on 07/10/2019.
//


#include <iostream>
#include "QPushButton"

using namespace std;


MasterLayout::MasterLayout(MyTomeo *tom):QLayout() {
    myTomeo = tom;
}


MyTomeo *MasterLayout::getTomeo(){
    return myTomeo;
}

void MasterLayout::clickTvShow(string pagename, string data) {
    cout << "registered clicker" << endl;
    MyTomeo *tom = getTomeo();
    if(pagename == "SeasonsView"){
        tom->setTvShow_id(data);
    }
    else if (pagename == "PlayerFull"){
        tom->setVideoDisplay_path(data);
    }
    tom->changePage(pagename);
}

void MasterLayout::backPage(string pagename) {
    cout << "registered clicker" << endl;
    MyTomeo *tom = getTomeo();
    tom->changePage(pagename);
}


// following methods provide a trivial list-based implementation of the QLayout class
int MasterLayout::count() const {
    return list_.size();
}

QLayoutItem *MasterLayout::itemAt(int idx) const {
    return list_.value(idx);
}

QLayoutItem *MasterLayout::takeAt(int idx) {
    return idx >= 0 && idx < list_.size() ? list_.takeAt(idx) : 0;
}

void MasterLayout::addItem(QLayoutItem *item) {
    list_.append(item);
}


QSize MasterLayout::sizeHint() const {
    return minimumSize();
}

QSize MasterLayout::minimumSize() const {
    return QSize(320,320);
}

MasterLayout::~MasterLayout() {
    QLayoutItem *item;
    while ((item = takeAt(0)))
        delete item;
}



void MasterLayout::setGeometry(const QRect &rect){
    cout<<"inside master 1"<<endl;
};
void MasterLayout::createWidgets(){
    cout<<"inside master 2"<<endl;
};


